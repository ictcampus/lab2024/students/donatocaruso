
INSERT INTO world (name, system) VALUES ('Mercurio', 'Sistema Solare');
INSERT INTO world (name, system) VALUES ('Venere', 'Sistema Solare');
INSERT INTO world (name, system) VALUES ('Terra', 'Sistema Solare');
INSERT INTO world (name, system) VALUES ('Marte', 'Sistema Solare');
INSERT INTO world (name, system) VALUES ('Giove', 'Sistema Solare');
INSERT INTO world (name, system) VALUES ('Saturno', 'Sistema Solare');
INSERT INTO world (name, system) VALUES ('Urano', 'Sistema Solare');
INSERT INTO world (name, system) VALUES ('Nettuno', 'Sistema Solare');

INSERT INTO authors(name, surname, nickname) VALUES ('Isaac', 'Asimov', 'Asimov');
INSERT INTO authors(name, surname, nickname) VALUES ('Gabriel', 'García Márquez', NULL);
INSERT INTO authors(name, surname, nickname) VALUES ('J.D.', 'Salinger', NULL);
INSERT INTO authors(name, surname, nickname) VALUES ('Mario', 'Torelli', NULL);
INSERT INTO authors(name, surname, nickname) VALUES ('Franz', 'Kafka', NULL);
INSERT INTO authors(name, surname, nickname) VALUES ('Jack', 'Kerouac', NULL);

INSERT INTO books(title, isbn, abstract, publisher, published_date, price, discount) VALUES ('Fondazione', '23963-2', 'La trilogia asimoviana della Fondazione, uscita in volumi separati dal 1951 al 1953', 'Mondatori', '1951-01-01', 9.00, 0);
INSERT INTO books(title, isbn, abstract, publisher, published_date, price, discount) VALUES ('Cent anni di solitudine', '978-8807880019', 'La storia della famiglia Buendía a Macondo, dal patriarca José Arcadio Buendía fino all ultima generazione.', 'Mondadori', '1967-05-30', 12.00, 0);
INSERT INTO books(title, isbn, abstract, publisher, published_date, price, discount) VALUES ('Il giovane Holden', '978-8806190476', 'Le vicende del giovane Holden Caulfield dopo essere stato espulso da un college.', 'Mondadori', '1951-07-16', 10.00, 0);
INSERT INTO books(title, isbn, abstract, publisher, published_date, price, discount) VALUES ('Storia degli etruschi', '978-8804316873', 'Un testo che offre una panoramica sulla civiltà etrusca, dalla loro origine fino alla conquista romana.', 'Laterza', '1970-01-01', 15.00, 0);
INSERT INTO books(title, isbn, abstract, publisher, published_date, price, discount) VALUES ('Il processo', '978-8804483393', 'La storia di Josef K., accusato di un crimine di cui non conosce né natura né motivo, e del suo processo.', 'Mondadori', '1925-04-26', 9.50, 0);
INSERT INTO books(title, isbn, abstract, publisher, published_date, price, discount) VALUES ('Sulla strada', '978-8817133283', 'Il viaggio on the road di Sal Paradise e Dean Moriarty attraverso gli Stati Uniti.', 'Rizzoli', '1957-09-05', 11.00, 0);

INSERT INTO book_author(book_id, author_id) VALUES (1, 1);
INSERT INTO book_author(book_id, author_id) VALUES (2, 2); -- Cent'anni di solitudine - Gabriel García Márquez
INSERT INTO book_author(book_id, author_id) VALUES (3, 3); -- Il giovane Holden - J.D. Salinger
INSERT INTO book_author(book_id, author_id) VALUES (4, 4); -- Storia degli etruschi - Mario Torelli
INSERT INTO book_author(book_id, author_id) VALUES (5, 5); -- Il processo - Franz Kafka

INSERT INTO users(username, password, role) VALUES ('admin', '$2a$10$yxZjf/A5ndtHO7zfZ3HsbuG7AZ2RCCMn75f7FoOO1E2x2/BtJFiK6', 'operator');

