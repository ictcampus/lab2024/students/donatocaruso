package com.ictcampus.lab.base.service.book;

import com.ictcampus.lab.base.repository.book.BookRepository;
import com.ictcampus.lab.base.control.book.model.NewBookRequest;
import com.ictcampus.lab.base.repository.author.entity.AuthorEntity;
import com.ictcampus.lab.base.repository.book.entity.BookEntity;
import com.ictcampus.lab.base.service.author.model.Author;
import com.ictcampus.lab.base.service.book.mapper.BookServiceStructMapper;
import com.ictcampus.lab.base.repository.author.AuthorRepository;
import com.ictcampus.lab.base.service.book.model.Book;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * TODO Add Class Description
 *
 * @author Emilio (FEj) Frusciante - Beta80Group
 * @since 1.0.0
 */
@Service
@AllArgsConstructor
@Slf4j
public class BookService {
	@Autowired
	private BookRepository bookRepository;

	@Autowired
	private AuthorRepository authorRepository;

	@Autowired
	private BookServiceStructMapper bookServiceStructMapper;

	public List<Book> getBooks() {
		List<BookEntity> bookEntities = bookRepository.findAll();
		return bookServiceStructMapper.toBooks(bookEntities);
	}

	public List<Book> getBooksCustom() {
		List<BookEntity> bookEntities = bookRepository.findBooksWithExtendData();
		return bookServiceStructMapper.toBooks(bookEntities);
	}


	public Book getBookById(Long id) {
		BookEntity bookEntity = bookRepository.findById(id).orElse(null);
		return bookServiceStructMapper.toBook(bookEntity);
	}

	public List<Book> searchBooksByTitle(String title) {
		List<BookEntity> bookEntities = bookRepository.findByTitleContainingIgnoreCase(title);
		return bookServiceStructMapper.toBooks(bookEntities);
	}

	public Book createBook(NewBookRequest newBookRequest) {
		BookEntity bookEntity = new BookEntity();
		bookEntity.setTitle(newBookRequest.getTitle());
		bookEntity.setIsbn(newBookRequest.getIsbn());
		bookEntity.setDescAbstract(newBookRequest.getDescAbstract());
		bookEntity.setDescription(newBookRequest.getDescription());
		bookEntity.setPublisher(newBookRequest.getPublisher());
		bookEntity.setPublishedDate(newBookRequest.getPublishedDate());
		bookEntity.setPrice(newBookRequest.getPrice());
		bookEntity.setDiscount(newBookRequest.getDiscount());

		// Mappa gli autori direttamente dalla richiesta
		List<AuthorEntity> authors = new ArrayList<>();
		for (Author author : newBookRequest.getAuthors()) {
			// Verifica se l'autore esiste già nel database
			Optional<AuthorEntity> existingAuthorOptional = authorRepository.findById(author.getId());
			if (existingAuthorOptional.isPresent()) {
				// Se l'autore esiste già, aggiungilo direttamente alla lista degli autori
				authors.add(existingAuthorOptional.get());
			} else {
				// Se l'autore non esiste, crea un nuovo autore e aggiungilo alla lista degli autori
				AuthorEntity newAuthorEntity = new AuthorEntity();
				newAuthorEntity.setName(author.getName());
				newAuthorEntity.setSurname(author.getSurname());
				// Altri campi dell'autore, se necessario

				// Salva il nuovo autore nel database
				AuthorEntity savedAuthorEntity = authorRepository.save(newAuthorEntity);
				authors.add(savedAuthorEntity);
			}
		}
		bookEntity.setAuthors(authors);

		// Salva il nuovo libro nel repository
		BookEntity savedBookEntity = bookRepository.save(bookEntity);

		// Ritorna il libro appena creato
		return bookServiceStructMapper.toBook(savedBookEntity);
	}


	public void deleteBookById(Long id) {
		bookRepository.deleteById(id);
	}
}
