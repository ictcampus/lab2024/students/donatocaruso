package com.ictcampus.lab.base.service.author;

import com.ictcampus.lab.base.control.author.model.AuthorResponse;
import com.ictcampus.lab.base.control.author.model.NewAuthorRequest;
import com.ictcampus.lab.base.repository.author.entity.AuthorEntity;
import com.ictcampus.lab.base.service.author.model.Author;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper
public interface AuthorServiceStructMapper {
    AuthorResponse toAuthorResponse(AuthorEntity authorEntity); // Aggiunto questo metodo

    @Mapping(target = "id", ignore = true)
    AuthorEntity toAuthorEntity(NewAuthorRequest newAuthorRequest);
}