package com.ictcampus.lab.base.service.author;

import com.ictcampus.lab.base.control.author.model.AuthorResponse;
import com.ictcampus.lab.base.control.author.model.NewAuthorRequest;
import com.ictcampus.lab.base.repository.author.AuthorRepository;
import com.ictcampus.lab.base.repository.author.entity.AuthorEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AuthorService {
    private final AuthorRepository authorRepository;
    private final AuthorServiceStructMapper authorServiceStructMapper;

    public AuthorResponse getAuthor(Long id) {
        AuthorEntity authorEntity = authorRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Author not found with id: " + id));
        return authorServiceStructMapper.toAuthorResponse(authorEntity);
    }

    public AuthorResponse createAuthor(NewAuthorRequest request) {
        AuthorEntity authorEntity = authorServiceStructMapper.toAuthorEntity(request);
        AuthorEntity savedAuthor = authorRepository.save(authorEntity);
        return authorServiceStructMapper.toAuthorResponse(savedAuthor);
    }

    public List<AuthorResponse> searchAuthorsBySurname(String surname) {
        List<AuthorEntity> authorEntities = authorRepository.findBySurnameContainingIgnoreCase(surname);
        return authorEntities.stream()
                .map(authorServiceStructMapper::toAuthorResponse)
                .collect(Collectors.toList());
    }

    public List<AuthorResponse> searchAuthorsByName(String name) {
        List<AuthorEntity> authorEntities = authorRepository.findByNameContainingIgnoreCase(name);
        return authorEntities.stream()
                .map(authorServiceStructMapper::toAuthorResponse)
                .collect(Collectors.toList());
    }
}
