package com.ictcampus.lab.base.service.book.model;

import lombok.Data;

/**
 * TODO Add Class Description
 *
 * @author Emilio (FEj) Frusciante - Beta80Group
 * @since 1.0.0
 */
@Data
public class Position {
	private Long id;
	private String floor;
	private String sector;
	private String rack;
	private String line;
}
