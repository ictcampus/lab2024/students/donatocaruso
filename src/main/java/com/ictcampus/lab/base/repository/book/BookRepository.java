package com.ictcampus.lab.base.repository.book;

import com.ictcampus.lab.base.repository.book.entity.BookEntity;
import com.ictcampus.lab.base.service.book.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * TODO Add Class Description
 *
 * @author Emilio (FEj) Frusciante - Beta80Group
 * @since 1.0.0
 */
@Repository
public interface BookRepository extends JpaRepository<BookEntity, Long>, BookRepositoryCustom {
	List<BookEntity> findByTitleContainingIgnoreCase(String title);
}

