package com.ictcampus.lab.base.repository.author;

import com.ictcampus.lab.base.repository.author.entity.AuthorEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AuthorRepository extends JpaRepository<AuthorEntity, Long> {
    List<AuthorEntity> findBySurnameContainingIgnoreCase(String surname);
    List<AuthorEntity> findByNameContainingIgnoreCase(String name);
}
