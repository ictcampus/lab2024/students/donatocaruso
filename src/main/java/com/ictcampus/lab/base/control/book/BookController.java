package com.ictcampus.lab.base.control.book;

import com.ictcampus.lab.base.control.book.mapper.BookControllerStructMapper;
import com.ictcampus.lab.base.control.book.model.BookResponse;
import com.ictcampus.lab.base.control.book.model.NewBookRequest;
import com.ictcampus.lab.base.service.book.BookService;
import com.ictcampus.lab.base.service.book.model.Book;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * TODO Add Class Description
 *
 * @author Emilio (FEj) Frusciante - Beta80Group
 * @since 1.0.0
 */
@RestController
@RequestMapping( "/api/v1/books" )
@AllArgsConstructor
@Slf4j
public class BookController {
	@Autowired
	private BookService bookService;

	@Autowired
	private BookControllerStructMapper bookControllerStructMapper;

	@GetMapping( value = "", produces = { MediaType.APPLICATION_JSON_VALUE } )
	public List<BookResponse> getBooks() {
		return bookControllerStructMapper.toBooks(bookService.getBooks());
	}

	@GetMapping( value = "/custom", produces = { MediaType.APPLICATION_JSON_VALUE } )
	public List<BookResponse> getBooksCustom() {
		return bookControllerStructMapper.toBooks(bookService.getBooksCustom());
	}

	@GetMapping( value = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE } )
	public BookResponse getBookById(@PathVariable Long id) {
		return bookControllerStructMapper.toBook(bookService.getBookById(id));
	}

	@GetMapping(value = "/search", produces = { MediaType.APPLICATION_JSON_VALUE })
	public List<BookResponse> searchBooksByTitle(@RequestParam String title) {
		return bookControllerStructMapper.toBooks(bookService.searchBooksByTitle(title));
	}

	@PostMapping(value = "", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<BookResponse> createBook(@RequestBody NewBookRequest newBookRequest) {
		Book createdBook = bookService.createBook(newBookRequest);
		BookResponse createdBookResponse = bookControllerStructMapper.toBook(createdBook);
		return new ResponseEntity<>(createdBookResponse, HttpStatus.CREATED);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Void> deleteBook(@PathVariable Long id) {
		bookService.deleteBookById(id);
		return ResponseEntity.noContent().build();
	}
}
