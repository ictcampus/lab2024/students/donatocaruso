package com.ictcampus.lab.base.control.book.mapper;

import com.ictcampus.lab.base.control.book.model.BookResponse;
import com.ictcampus.lab.base.control.book.model.NewBookRequest;
import com.ictcampus.lab.base.service.book.model.Book;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * TODO Add Class Description
 *
 * @author Emilio (FEj) Frusciante - Beta80Group
 * @since 1.0.0
 */
@Mapper
public interface BookControllerStructMapper {
	/**
	 * Converte un oggetto Book in un oggetto BookResponse.
	 * @param book l'oggetto Book da convertire
	 * @return l'oggetto BookResponse convertito
	 */
	BookResponse toBook(Book book);

	/**
	 * Converte una lista di oggetti Book in una lista di oggetti BookResponse.
	 * @param books la lista di oggetti Book da convertire
	 * @return la lista di oggetti BookResponse convertiti
	 */
	List<BookResponse> toBooks(List<Book> books);

	/**
	 * Converte un oggetto NewBookRequest in un oggetto Book.
	 * @param newBookRequest l'oggetto NewBookRequest da convertire
	 * @return l'oggetto Book convertito
	 */
	Book toBook(NewBookRequest newBookRequest);
}