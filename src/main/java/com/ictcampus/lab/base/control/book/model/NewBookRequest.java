package com.ictcampus.lab.base.control.book.model;

import com.ictcampus.lab.base.service.author.model.Author;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Data
public class NewBookRequest {
    private String title;
    private String isbn;
    private String descAbstract;
    private String description;
    private String publisher;
    private LocalDate publishedDate;
    private BigDecimal price;
    private BigDecimal discount;
    private List<Author> authors; // Lista degli autori del libro
}
