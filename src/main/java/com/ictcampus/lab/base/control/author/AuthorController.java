package com.ictcampus.lab.base.control.author;

import com.ictcampus.lab.base.control.author.model.AuthorResponse;
import com.ictcampus.lab.base.control.author.model.NewAuthorRequest;
import com.ictcampus.lab.base.service.author.AuthorService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/authors")
@RequiredArgsConstructor
public class AuthorController {
    private final AuthorService authorService;

    @GetMapping("/{id}")
    public ResponseEntity<AuthorResponse> getAuthor(@PathVariable Long id) {
        AuthorResponse author = authorService.getAuthor(id);
        return ResponseEntity.ok(author);
    }

    @PostMapping
    public ResponseEntity<AuthorResponse> createAuthor(@RequestBody NewAuthorRequest request) {
        AuthorResponse createdAuthor = authorService.createAuthor(request);
        return ResponseEntity.status(HttpStatus.CREATED).body(createdAuthor);
    }

    @GetMapping("/searchBySurname")
    public ResponseEntity<List<AuthorResponse>> searchAuthorsBySurname(@RequestParam String surname) {
        List<AuthorResponse> authors = authorService.searchAuthorsBySurname(surname);
        return ResponseEntity.ok(authors);
    }

    @GetMapping("/searchByName")
    public ResponseEntity<List<AuthorResponse>> searchAuthorsByName(@RequestParam String name) {
        List<AuthorResponse> authors = authorService.searchAuthorsByName(name);
        return ResponseEntity.ok(authors);
    }
}
