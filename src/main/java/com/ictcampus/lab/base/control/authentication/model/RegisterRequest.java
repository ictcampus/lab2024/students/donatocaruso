package com.ictcampus.lab.base.control.authentication.model;

import lombok.Value;
import lombok.extern.jackson.Jacksonized;

/**
 * TODO Add Class Description
 *
 * @autor Emilio (FEj) Frusciante - Beta80Group
 * @since 1.0.0
 */
@Value
@Jacksonized
public class RegisterRequest {
    String username;
    String password;
    String role;
}
