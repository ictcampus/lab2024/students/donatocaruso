package com.ictcampus.lab.base.control.author.model;

import lombok.Data;

@Data
public class NewAuthorRequest {
    private String name;
    private String surname;
    private String nickname;
}