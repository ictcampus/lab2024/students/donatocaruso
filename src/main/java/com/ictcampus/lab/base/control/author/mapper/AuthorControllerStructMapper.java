package com.ictcampus.lab.base.control.author.mapper;

import com.ictcampus.lab.base.control.author.model.AuthorResponse;
import com.ictcampus.lab.base.control.author.model.NewAuthorRequest;
import com.ictcampus.lab.base.repository.author.entity.AuthorEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface AuthorControllerStructMapper {
    AuthorResponse toAuthorResponse(AuthorEntity authorEntity);

    @Mapping(target = "id", ignore = true)
    AuthorEntity toAuthorEntity(NewAuthorRequest newAuthorRequest);

    List<AuthorResponse> toAuthorResponses(List<AuthorEntity> authorEntities);
}